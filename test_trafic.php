<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Calcul d'itinéraire Google Map Api v3</title>
  <!--<link rel="stylesheet" href="https://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css" type="text/css" />-->

  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>
<style>
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: 'Montserrat', sans-serif;
  }
  #map {

    height: 70%;
    width: 40%;
    border: solid beige 3px;
    -webkit-border-radius: 10px;
    -webkit-box-shadow: 1px 1px 1px rgba(160,160, 150, 0.5); ;
  }

  #title{
    width: 100%;
    font-family: 'Montserrat', sans-serif;


    font-size: 18px;
    background-color:beige;
    color: darkgray;
    text-align: center;
  }


  .pac-card {
    padding: 10px;
    -webkit-border-radius: 10px;
    font-family: 'Montserrat', sans-serif;
    font-size: 12px;
    width: 80%;
    background-color: rgba(100,100,100,0.6);
    color: whitesmoke;
  }

  #pac-container {
    display: inline-block;

    padding-bottom: 12px;
    margin-right: 12px;

  }

  .pac-controls {
    display: inline-block;
    margin-left: 20px;
    padding: 5px 11px;
  }

  .pac-controls label {
    font-family: 'Montserrat', sans-serif;
    font-size:15px;
    font-weight: 300;
    color: whitesmoke;
    text-shadow: 1px 1px 1px #666666;
  }

  #pac-input {
    font-family: 'Montserrat', sans-serif;
    width: 420px;
    font-size: 14px;
    margin-left: 0px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;


  }

  #pac-input:focus {
    border-color: #4d90fe;
    -webkit-border-radius: 3px;
  }

  input[type="button"] {
    margin-left: 15px;
    display:inline-block;
    font-family: 'Montserrat', sans-serif;
    border:none;
    -webkit-border-radius: 10px;
    width: 125px;
    height: 35px;
    font-size: 9px;
    text-transform: uppercase;
    font-weight: 500;
    color: #000;
    background-color: #fff;
    border-radius: 45px;
    box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
    transition: all 0.3s ease 0s;
    cursor: pointer;
  }

  input[type="button"]:focus{
    outline: none;
  }


  input[type="button"]:hover {
    background-color: #ff5722;
    box-shadow: 0px 15px 20px rgba(119, 29, 0, 0.73);
    color: #fff;
    transform: translateY(-2px);
  }


#panel{
  display: none;
}



</style>
<body>

  <div class="pac-card" id="pac-card">

    <div id="title">
      Trafic
    </div>

    <div id="label">
      <label>Point de départ :</label>
      <input type="text" name="origin" id="origin">
      <input id="btn_geoloc" type="button" onclick="geoloc();" value="Géolocalisation"/>
      <label>Destination :</label>
      <input type="text" name="destination" id="destination">

      <input type="button" value="Calculer l'itinéraire" onclick="javascript:calculate()">
      <input type="button" value="Afficher les étapes" onclick="toggle_visibility('panel')">
    </div>
    <div id="pac-container">
      <input id="pac-input" type="text" placeholder="Entrer une adresse">
    </div>
  </div>

  <div id="map"></div>
  <div id="infowindow-content">
    <img src="" width="16" height="16" id="place-icon">
    <span id="place-name"  class="title"></span><br>
    <span id="place-address"></span>
  </div>
  <div id="panel"></div>

  <!-- Include Javascript -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/ui/1.8.12/jquery-ui.min.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAID1qsUH2d6ajd83Oo6kL9ir5X0X_lF2k&language=fr&libraries=places"></script>

  <script type="text/javascript" src="modules/trafic/js/trafic.js"></script>

</body>
</html>
