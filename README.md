____

**Module Trafic**
-----------------
v0.1
_____
Installation
============
1. Copier le dossier trafic dans le dossier "modules/" à la racine du site

2. Coller la route dans le fichier index.php à la racine du site :
```$route['trafic'] = array(
		'c' => "CtrlTrafic",
		'm' => "getTrafic",
		'a' => "",
		'module' => "trafic"
);```

3. Éditer *themes/$monTheme/partials/nav.php* pour y ajouter un élément au menu (bouton, lien...) pointant vers le module trafic. Exemple :
```<a href="trafic">Trafic</a>```

4. Clé d'accès API Google Map
	* Pour obtenir une clé API Google Map il faut aller sur https://developers.google.com/maps/documentation/javascript/get-api-key, puis suivre les instructions.
	   -> sur la page : https://console.developers.google.com/apis/dashboard
	      activer :
	        o-Google Places API Web Service Private API
            o-Google Maps JavaScript API
            o-Google Maps Directions API
            o-Google Maps Geocoding API
            o-Google Maps Embed API
            o-Google Maps Roads API

	* Pour que l'API Goole Map fonctionne il faut renseigner la clé api dans le script *modules/trafic/partials/trafic_trafic.php* en ajoutant ce code à la fin du fichier :
```<script src="http://maps.google.com/maps/api/js?key=ENTRER_VOTRE_CLE_API_ICI&language=fr&libraries=places"></script>```

____
Fonctionnalités
===============
* Flux d'infos :
	* http://www.autorouteinfo.fr/rssbreves.ashx
	* http://www.securite-routiere.gouv.fr/feed/rss/actualites
* Intégration googleAPI
	* Recherche de lieu
	* Recherche d'itinéraire
* Géolocalisation par le navigateur

