<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Calcul d'itinéraire Google Map Api v3</title>
  <!--<link rel="stylesheet" href="https://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css" type="text/css" />-->

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Footer</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

<style>

    @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400');


html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: 'Montserrat', sans-serif;
  }
 
#navFooter{
    background-color: #0E1C36;
}
    #textFooter{
        font-size: 12px;
        font-weight:300;
        color: white;
        padding-top: 5px;
        letter-spacing: 0.3px;
    }
#myNavbar{
    margin-right: 50px;
    margin-left: 50px;
    text-align: justify;
}
    #logoSocial{
        padding-top: 5px;
        width:20%;
        margin-right: auto;
        margin-left: auto;
    }
    #mentions{
        font-size: 10px;
        width:30%;
        margin-right: auto;
        margin-left: auto;
        letter-spacing: 0.5px;

    }
    a{
        text-decoration: none;
        color: lightblue;

    }
    #btnFooter{
        background-color: white;

    }




</style>
<body>
<nav class="navbar navbar-inverse navbar-fixed-bottom">
    <div class="container-fluid" id="navFooter">
        <div class="navbar-header">
            <button id="btnFooter" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <i class="material-icons">menu</i>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="logoSocial">
            <a href="https://fr-fr.facebook.com/AFPA.JEUNES/" target="_blank"><img src="themes/default/images/social/Facebook.png"></a>
            <a href="https://twitter.com/afpa_formation" target="_blank"><img src="themes/default/images/social/Twitter.png"></a>
            <a href="https://www.youtube.com/user/AfpaWebTv" target="_blank"><img src="themes/default/images/social/Youtube.png"></a>
            <a href="https://www.linkedin.com/company/afpa" target="_blank"><img src="themes/default/images/social/Linkedin.png"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <p id="textFooter">
            L’Agence nationale pour la formation professionnelle des adultes (Afpa) est un organisme français de formation professionnelle
            au service des Régions, de l’État, des branches professionnelles et des entreprises. Membre du Service public
            de l'emploi, l'Afpa, constituée en association avant de devenir en 2017 un EPIC propose des formations
            qualifiantes sanctionnées par un titre professionnel du ministère du Travail.
            Adresse : 73 Rue Saint-Jean, 31130 Balma, Téléphone : 3936</p>
            <div id="mentions">
                <p><a href="#">CGV |</a>
                <a href="#"> Contact |</a>
                <a href="#"> Plan du site |</a>
                <a href="#"> Légale |</a>
                <a href="#"> Rss</a></p>
            </div>

        </div>
    </div>
</nav>

<div id="headerPage"></div>
<div id="contentPage"></div>




</body>

  <!-- Include Javascript -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/ui/1.8.12/jquery-ui.min.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAID1qsUH2d6ajd83Oo6kL9ir5X0X_lF2k&language=fr&libraries=places"></script>

  <script type="text/javascript" src="modules/trafic/js/trafic.js"></script>


</html>
