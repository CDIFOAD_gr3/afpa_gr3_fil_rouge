<?php
/**
 * chargement automatique des fichiers de Class
 * @param string $class_name
 */

function getModules($modulesRoot = 'modules'){

	//vérifie le trailingslash
	if ($modulesRoot[strlen($modulesRoot)-1] !== '/')
		$modulesRoot=$modulesRoot.'/';

	//renvoie les dossiers contenus dans /modules/
	$res = array_diff(scandir($modulesRoot),array('..', '.'));

	//construit le tableau modules

	$r[]=""; //si pas de module en url, sinon, modules :
	foreach ($res as $re) {
		if (is_dir($modulesRoot.$re)&& ($re[0]!=='_')) //un dossier module préfixé d'un underscore est inactif ?
			$r[]=$modulesRoot.$re.'/';
	}

	return($r);
}

function __autoload($class_name = ""){


	$modules=getModules();

//	$modules = array(
//			"",
//			"modules/contact/",
//			"modules/meteo/",
//			"modules/trafic/"
//	);

	$repertoires = array(
			"controllers/",
			"models/",
			"views/",
			"system/",
			"config/"
	);
	foreach ($modules as $module){

		foreach($repertoires as $repertoire){
			$file = "{$module}{$repertoire}{$class_name}.php";
			if(file_exists($file)){
				include_once($file);
			}
		}
	}
}
?>