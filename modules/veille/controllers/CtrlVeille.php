<?php
class CtrlVeille
{
    var $view;
    var $mdl;

    //valeurs par défaut, pourront être mises dans Config;
    //etiquette | url | limite de resultat (-1 = tout)
    var $src = [
        ["veilleTechno", "http://feeds.lafermeduweb.net/LaFermeDuWeb", 5]
    ];

    function __construct()
    {
        $this->view= new ViewVeille;
        $this->mdl = new ModelVeille;

        //on écrase avec la config, si elle existe...
        if (isset(Config::$src_xml_trafic)){
            $this->src= Config::$src_xml_trafic;
        }
    }

    function getVeille(){
        $this->view->afficherVeille($this->mdl->getNews($this->src));
    }
}
