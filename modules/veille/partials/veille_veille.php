<style xmlns="http://www.w3.org/1999/html">@import "<?= Config::$base_href ?>modules/trafic/css/trafic.css"</style>



<script>
    function findPos(obj) {
        var curtop = 0;
        if (obj.offsetParent) {
            do {
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return [curtop];
        }
    }
</script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<!--<style>@import "https://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css" type="text/css"</style>-->

<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

<div id="container">
<div id="titre">Infos Veille Techno !</div>
    <div id="fluxRss">
<?php
foreach ($datas['sources'] as $src){
$i = 0; //RaZ à chaque source
$maxRss = 3; // nbre d'article par flux
    if ($datas['errors'][$src[0]]==false){
        foreach ($datas['datas'][$src[0]] as $article){
            $i++;
            if ($i <= $maxRss)
            {
                //affiche les $maxRss premiers de chaque flux
?>


            <button id="btnNews" data-toggle="collapse" data-target="#<?php echo $src[0]."_".$i;?>">
                <?php echo $article['title'];?><p><i class="material-icons">arrow_drop_down</i><p></button>

            <div id="<?php echo $src[0]."_".$i;?>" class="collapse maClasse">
                <?php echo $article['date']. $article['desc']?>
                <?php echo '<a href="'.$article['lnk'].'"target="_blank">Lien article</a>';?>

            </div>


<?php
            } //if ($i <= $maxRss)
        } //foreach ($datas['datas'][$src[0]] as $article){
    }//if ($datas['errors'][$src[0]]==false){
} //foreach ($datas['sources'] as $src){

?>

</div>
