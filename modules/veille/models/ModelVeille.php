<?php
class ModelVeille
{
    var $dao;

    //agrès tous les résultats avant dump dans tableau final
    var $news = array();

    //lire sous forme "cle" -> valeur

    //pour la lecture du tableau final, contient un tableau[0] aka sources
    //cle
    //url source
    //tableau[1]
    //source[]
    //title     (str)
    //desc      (str)
    //lnk       (str)
    //pubDate   (str)
    var $result = array();

    function getNews($src)
    {
        $this->dao = new DAO;

        //return $this->dao->wsQuery($url);

        foreach ($src as $s) {
            //var_dump($s[1]);
            $rss=$this->dao->wsQuery($s[1]);
            if ($rss !== false) {
                $objRss = new SimpleXMLElement($rss);
                $i = 0;
                foreach ($objRss->channel->item as $itm) {
                    if ($s[2] ==-1 || $i<$s[2]) {
                        $this->news[$s[0]]['itm' . $i]['title'] = "" . $itm->title;
                        $this->news[$s[0]]['itm' . $i]['lnk'] = "" . $itm->link;
                        $this->news[$s[0]]['itm' . $i]['desc'] = trim("" . $itm->description);
                        $this->news[$s[0]]['itm' . $i]['date'] = date('d/m/Y', strtotime($itm->pubDate)); //renvoie une date formatée
                        //$this->news[$s[0]]['itm' . $i]['date'] = "" . $itm->pubDate;
                        // var_dump($itm->title);
                        $i++;
                    }
                }
                $this->result['errors'][$s[0]] = false;
            }
            else
            {
                $this->result['errors'][$s[0]] = true;
            }
        }
        $this->result['sources'] = $src;
        $this->result['datas'] = $this->news;
        return $this->result;
    }
    /*  retourne :
     *
        'errors' =>
            array (size=2)
                'securiteRoutiere' => boolean false //si true, sera absent de 'datas"
                'autorouteInfo' => boolean false
        'sources' =>
            array (size=2)
                0 =>
                array (size=2)
                    0 => string 'securiteRoutiere' (length=16)
                    1 => string 'http://www.securite-routiere.gouv.fr/feed/rss/actualites' (length=56)
                1 =>
                array (size=2)
                    0 => string 'autorouteInfo' (length=13)
                    1 => string 'http://www.autorouteinfo.fr/rssbreves.ashx' (length=42)
        'datas' =>
            array (size=2)
                'securiteRoutiere' =>
                    array (size=50)
                        'itm0' =>
                        array (size=4)
                        ...
                'autorouteInfo' =>
                    array (size=10)
                        'itm0' =>
                            array (size=4)
                        ...

     */
}
