<?php
/**
 * Created by PhpStorm.
 * User: Sam
 * Date: 08/06/2017
 * Time: 11:01
 */
$routes['contact'] = array(
    'c' => "CtrlContact",
    'm' => "getForm",
    'a' => "",
    'module' => "contact"
);
$routes['savecontact'] = array(
    'c' => "CtrlContact",
    'm' => "enregistrerForm",
    'a' => "",
    'module' => "contact"
);
$routes['stats'] = array(
    'c' => "CtrlContact",
    'm' => "getStats",
    'a' => "",
    'module' => "contact"
);