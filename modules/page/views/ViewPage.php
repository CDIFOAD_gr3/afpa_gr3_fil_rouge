<?php 
class ViewPage{
	
	
	
	public function afficherPage($datas=array(),$partial="accueil"){
		$params = array(
				'content' => "{$partial}"
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
	public function afficherErreurDeSaisie($datas=array(),$partial='erreurBdd'){
		$params = array(
				'content' => "{$partial}"
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
}
