<?php 
class CtrlMeteo{
	
	private $view;
	private $model;
	private $datas;
	
	public function __construct(){
		$this->view = new ViewMeteo;
		$this->model = new ModelMeteo;
		$this->datas = array();
	}
	

	public function getMeteo(){
		$ville = array_key_exists('ville', $_POST) ? $_POST['ville'] : false;
		
		if($ville){
			$this->datas = $this->model->meteoVille($ville);
			if($this->verifier()){
				$this->view->afficherMeteo($this->datas);
			}
			else{
				$this->view->afficherErreur($this->datas);
			}
		}
		else{
			$this->view->afficherErreur(array('erreur'=>"Erreur de saisie de la ville"));
		}
		
	}

	private function verifier(){
		$ok = false;
		if(array_key_exists('erreur',$this->datas)){
			$ok = false;
		}
		else{
			$ok = true;
		}
		return $ok;
	}
	

	public function getWidgetMeteo(){
		if($this->verifierSessionMeteo()){
			$this->view->afficherWidgetMeteo();
		}
		else{
			$this->view->afficherWidgetErreur();
		}
	}
	
	private function verifierSessionMeteo(){
		if(array_key_exists('meteo',$_SESSION) && !empty($_SESSION['meteo'])){
			return true;
		}
		else{
			return false;
		}
	}
}
?>