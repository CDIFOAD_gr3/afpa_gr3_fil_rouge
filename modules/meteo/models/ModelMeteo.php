<?php

class ModelMeteo{

	private $dao;

	/**
	 * constructeur
	 */
	public function __construct(){
		$this->dao = new DAO;
	}

	public function meteoVille($ville){
		$url = Config::$url_meteo . $ville;
		$json = $this->dao->wsQuery($url);
		return $this->traiter($json);
	}
	
	private function traiter($json){
		$reponse = array();
		$array = json_decode($json,true);
		if(array_key_exists('errors',$array)){
			$reponse['erreur'] = "Ville ou coordonnée introuvable";
		}
		else{
			$_SESSION['meteo'] = $array;
			$reponse = $array;
		}
		return $reponse;
	}
}
?>