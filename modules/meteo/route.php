<?php
/**
 * Created by PhpStorm.
 * User: Sam
 * Date: 08/06/2017
 * Time: 11:02
 */
$routes['meteo'] = array(
    'c' => "CtrlMeteo",
    'm' => "getMeteo",
    //'a' => "toulouse",
    'a' => "",
    'module' => "meteo"
);
$routes['widgetmeteo'] = array(
    'c' => "CtrlMeteo",
    'm' => "getWidgetMeteo",
    'a' => "",
    'module' => "meteo"
);