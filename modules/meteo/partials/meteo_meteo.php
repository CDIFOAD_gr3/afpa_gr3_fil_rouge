<?php 
//var_dump($datas['fcst_day_0']['hourly_data']);
?>
<?php  
// Equivalent javascript
/*
	contents+="<h1>"+o.city_info.name+"</h1>";
	contents+="<h2>"+o.current_condition.date+"</h2>";
	contents+="<table id='prev_jours'><tr><td>Jour</td><td>Icône</td><td>Cond.</td><td>Tmin</td><td>Tmax</td></tr>";
	for(i=0;i<5;i++)
	{
		contents+=
			'<tr>'+
			'<td>'+o['fcst_day_'+i]['day_long']+'</td>'+
			'<td><img src="'+o['fcst_day_'+i]['icon']+'"/></td>'+
			'<td>'+o['fcst_day_'+i]['condition']+'</td>'+
			'<td>'+o['fcst_day_'+i]['tmin']+'°C</td>'+
			'<td>'+o['fcst_day_'+i]['tmax']+'°C</td>'+
			'</tr>';
	}
	contents+="</table>";
*/	
?>

 <h1><?= $datas['city_info']['name'] ?></h1>
 <h2><?= $datas['current_condition']['date'] ?></h2>
 <table class="table">
 	<tr><td>Jour</td><td>Icône</td><td>Cond.</td><td>Tmin</td><td>Tmax</td></tr>
 	<?php for($i=0;$i<5;$i++) : ?>
 		<tr>
 			<td><?= $datas["fcst_day_{$i}"]['day_long'] ?></td>
 			<td><img src='<?= $datas["fcst_day_{$i}"]['icon'] ?>'/></td>
 			<td><?= $datas["fcst_day_{$i}"]['condition'] ?></td>
 			<td><?= $datas["fcst_day_{$i}"]['tmin'] ?>°C</td>
 			<td><?= $datas["fcst_day_{$i}"]['tmax'] ?>°C</td>
 		</tr>
 	<?php endfor; ?>
 </table>