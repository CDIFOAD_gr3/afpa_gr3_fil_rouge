/**
	Requete en AJAX pour charger le widget meteo

*/

$(document).ready(function() {
	
	myAjax({
		'url' 		: "widgetmeteo",
		'type' 		: "POST",
		'dataType' 	: "html",
		'data'		: {},
		'callbackSuccess' : function(result){
			$("#widgetmeteo").html(result);
		},
		'callbackError' : function(result){
			console.log(result);
		}
	});
	
});



function myAjax(obj){
	$.ajax({
		url : obj.url,
		type : obj.type,
		dataType : obj.dataType,
		async	:	true,
		data: obj.data,
		success : function(result){
			obj.callbackSuccess(result);
		},
		error : function(result){
			obj.callbackError(result);
		},
		complete : function(result){
			console.log('fini');
		}
	});
}