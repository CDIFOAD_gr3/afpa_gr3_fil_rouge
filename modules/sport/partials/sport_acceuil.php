<form class="navbar-form navbar-left" action="sport" method="post">
    <div class="btn-group" style="float: left">
        <button class="btn btn-default btn-ms dropdown-toggle" type="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sports <span class="caret"></span>
        </button>
        <ul id="dropdown-sport" class="dropdown-menu">
            <?php echo $_SESSION['dropdown-sport']?>
        </ul>
    </div>

    <div class="form-group">
        <input type="text" name="sport" class="form-control"
               placeholder="Saisir un critère">
    </div>
    <button type="submit" class="btn btn-primary">Recherche</button>
</form>

<br><br><br>

<?php include "modules/sport/partials/sport_".$content['content'].".php"; ?>