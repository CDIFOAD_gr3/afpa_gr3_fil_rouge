<style>
    form#monform div label::after {
        content: ' : ';
    }
</style>

<h1>Formulaire de sport</h1>
<div class="jumbotron">
    <form id="monform" class="form-horizontal" method="post" action="newSport">


        <div class="form-group">
            <label class="col-xs-12 col-sm-3 control-label" for="sport">Sport</label>
            <div class="col-xs-12 col-sm-7">
                <input class="form-control" name="newSport" id="sport" value="" placeholder="Nouveau Sport"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-7 col-sm-offset-3">
                <button type="reset" class="btn btn-default">Annuler</button>
                <button id="valider" type="submit" style="float:right;" class="btn btn-success">Valider</button>
            </div>
        </div>
    </form>
</div>