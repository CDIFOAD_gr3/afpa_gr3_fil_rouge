-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 03 Juin 2017 à 10:13
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test_integration`
--

-- --------------------------------------------------------

--
-- Structure de la table `sport_est_relie_a`
--

CREATE TABLE `sport_est_relie_a` (
  `supprimer` int(11) NOT NULL,
  `id_sport` int(11) NOT NULL,
  `id_fluxRSS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sport_est_relie_a`
--

INSERT INTO `sport_est_relie_a` (`supprimer`, `id_sport`, `id_fluxRSS`) VALUES
(0, 1, 1),
(0, 1, 2),
(0, 2, 3),
(0, 2, 4),
(0, 3, 5),
(0, 3, 6),
(0, 4, 7),
(0, 4, 8),
(0, 5, 9),
(0, 5, 10),
(0, 6, 11),
(0, 6, 12),
(0, 7, 13),
(0, 8, 14),
(0, 8, 15),
(0, 9, 16),
(0, 9, 17),
(0, 10, 18),
(0, 10, 19),
(0, 11, 20),
(0, 11, 21),
(0, 12, 22),
(0, 12, 23),
(0, 13, 24),
(0, 13, 25),
(0, 14, 26),
(0, 14, 27),
(0, 15, 28),
(0, 15, 29),
(0, 16, 30),
(0, 17, 31),
(0, 17, 32),
(0, 18, 33),
(0, 19, 34),
(0, 19, 35),
(0, 20, 36),
(0, 20, 37),
(0, 21, 38),
(0, 21, 39);

-- --------------------------------------------------------

--
-- Structure de la table `sport_fluxrss`
--

CREATE TABLE `sport_fluxrss` (
  `id_fluxRSS` int(11) NOT NULL,
  `nom_site` varchar(100) NOT NULL,
  `nom_flux` varchar(100) NOT NULL,
  `url_flux` varchar(500) NOT NULL,
  `supprimer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sport_fluxrss`
--

INSERT INTO `sport_fluxrss` (`id_fluxRSS`, `nom_site`, `nom_flux`, `url_flux`, `supprimer`) VALUES
(1, 'bfmtv', 'football', 'http://rmcsport.bfmtv.com/rss/football/', 0),
(2, 'lequipe', 'football', 'https://www.lequipe.fr/rss/actu_rss_Football.xml', 0),
(3, 'bfmtv', 'rugby', 'http://rmcsport.bfmtv.com/rss/rugby/', 0),
(4, 'lequipe', 'rugby', 'https://www.lequipe.fr/rss/actu_rss_Rugby.xml', 0),
(5, 'bfmtv', 'tennis', 'http://rmcsport.bfmtv.com/rss/tennis/', 0),
(6, 'lequipe', 'tennis', 'https://www.lequipe.fr/rss/actu_rss_Tennis.xml', 0),
(7, 'bfmtv', 'handball', 'http://rmcsport.bfmtv.com/rss/handball/', 0),
(8, 'lequipe', 'handball', 'https://www.lequipe.fr/rss/actu_rss_Hand.xml', 0),
(9, 'bfmtv', 'athletisme', 'http://rmcsport.bfmtv.com/rss/athletisme/', 0),
(10, 'lequipe', 'athletisme', 'https://www.lequipe.fr/rss/actu_rss_Athletisme.xml', 0),
(11, 'bfmtv', 'basket', 'http://rmcsport.bfmtv.com/rss/basket/', 0),
(12, 'lequipe', 'basket', 'https://www.lequipe.fr/rss/actu_rss_Basket.xml', 0),
(13, 'bfmtv', 'boxe', 'http://rmcsport.bfmtv.com/rss/boxe/', 0),
(14, 'bfmtv', 'cyclisme', 'http://rmcsport.bfmtv.com/rss/cyclisme/', 0),
(15, 'lequipe', 'cyclisme', 'https://www.lequipe.fr/rss/actu_rss_Cyclisme.xml', 0),
(16, 'bfmtv', 'escrime', 'http://rmcsport.bfmtv.com/rss/escrime/', 0),
(17, 'lequipe', 'escrime', 'https://www.lequipe.fr/rss/actu_rss_Escrime.xml', 0),
(18, 'bfmtv', 'formule1', 'http://rmcsport.bfmtv.com/rss/f1/', 0),
(19, 'lequipe', 'formule1', 'https://www.lequipe.fr/rss/actu_rss_F1.xml', 0),
(20, 'bfmtv', 'golf', 'http://rmcsport.bfmtv.com/rss/golf/', 0),
(21, 'lequipe', 'golf', 'https://www.lequipe.fr/rss/actu_rss_Golf.xml', 0),
(22, 'bfmtv', 'jo', 'http://rmcsport.bfmtv.com/rss/jo/', 0),
(23, 'lemonde', 'jo', 'http://www.lemonde.fr/jeux-olympiques/rss_full.xml', 0),
(24, 'bfmtv', 'judo', 'http://rmcsport.bfmtv.com/rss/judo/', 0),
(25, 'lequipe', 'judo', 'https://www.lequipe.fr/rss/actu_rss_Judo.xml', 0),
(26, 'bfmtv', 'moto', 'http://rmcsport.bfmtv.com/rss/auto-moto/', 0),
(27, 'lequipe', 'moto', 'https://www.lequipe.fr/rss/actu_rss_Moto.xml', 0),
(28, 'bfmtv', 'natation', 'http://rmcsport.bfmtv.com/rss/natation/', 0),
(29, 'lequipe', 'natation', 'https://www.lequipe.fr/rss/actu_rss_Natation.xml', 0),
(30, 'sportsfr', 'nba', 'http://www.sports.fr/fr/cmc/nba/rss.xml', 0),
(31, 'sportsfr', 'rallye', 'http://www.sports.fr//fr/cmc/rallyes/rss.xml', 0),
(32, 'lequipe', 'rallye', 'https://www.lequipe.fr/rss/actu_rss_Rallye.xml', 0),
(33, 'lequipe', 'ski', 'https://www.lequipe.fr/rss/actu_rss_Ski.xml', 0),
(34, 'bfmtv', 'voile', 'http://rmcsport.bfmtv.com/rss/voile/', 0),
(35, 'lequipe', 'voile', 'https://www.lequipe.fr/rss/actu_rss_Voile.xml', 0),
(36, 'bfmtv', 'volley', 'http://rmcsport.bfmtv.com/rss/volley/', 0),
(37, 'lequipe', 'volley', 'https://www.lequipe.fr/rss/actu_rss_Volley.xml', 0),
(38, 'bfmtv', 'general', 'http://rmcsport.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/', 0),
(39, 'lequipe', 'general', 'http://www.lequipe.fr/rss/actu_rss.xml', 0);

-- --------------------------------------------------------

--
-- Structure de la table `sport_sport`
--

CREATE TABLE `sport_sport` (
  `id_sport` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `supprimer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sport_sport`
--

INSERT INTO `sport_sport` (`id_sport`, `nom`, `supprimer`) VALUES
(1, 'Football', 0),
(2, 'Rugby', 0),
(3, 'Tennis', 0),
(4, 'Handball', 0),
(5, 'Athletisme', 0),
(6, 'Basket', 0),
(7, 'Boxe', 0),
(8, 'Cyclisme', 0),
(9, 'Escrime', 0),
(10, 'Formule1', 0),
(11, 'Golf', 0),
(12, 'JO', 0),
(13, 'Judo', 0),
(14, 'Moto', 0),
(15, 'Natation', 0),
(16, 'NBA', 0),
(17, 'Rallye', 0),
(18, 'Ski', 0),
(19, 'Voile', 0),
(20, 'Volley', 0),
(21, 'General', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `sport_est_relie_a`
--
ALTER TABLE `sport_est_relie_a`
  ADD PRIMARY KEY (`id_sport`,`id_fluxRSS`),
  ADD KEY `FK_sport_est_relie_a_id_fluxRSS` (`id_fluxRSS`);

--
-- Index pour la table `sport_fluxrss`
--
ALTER TABLE `sport_fluxrss`
  ADD PRIMARY KEY (`id_fluxRSS`);

--
-- Index pour la table `sport_sport`
--
ALTER TABLE `sport_sport`
  ADD PRIMARY KEY (`id_sport`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `sport_fluxrss`
--
ALTER TABLE `sport_fluxrss`
  MODIFY `id_fluxRSS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `sport_sport`
--
ALTER TABLE `sport_sport`
  MODIFY `id_sport` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `sport_est_relie_a`
--
ALTER TABLE `sport_est_relie_a`
  ADD CONSTRAINT `FK_sport_est_relie_a_id_fluxRSS` FOREIGN KEY (`id_fluxRSS`) REFERENCES `sport_fluxrss` (`id_fluxRSS`),
  ADD CONSTRAINT `FK_sport_est_relie_a_id_sport` FOREIGN KEY (`id_sport`) REFERENCES `sport_sport` (`id_sport`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
