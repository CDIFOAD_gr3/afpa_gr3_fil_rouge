Module sport version 1.0.1.

Fonctionnalit�s disponible :
-

		**************************
		***    Installation    ***
		**************************
		***     Le module      ***
		**************************

Copier le dossier sport dans le r�pertoire modules de votre application. (version�: tp_mvc_7)


		**************************
		*** La base de donn�es ***
		**************************

Dans le dossier bdd se trouve le fichier module_sport.sql
Il doit �tre importer dans votre base de donn�es utilis�s par l'application.


		*****************************
		*** Modifications du code ***
		*****************************
		***   Fichier index.php   ***
		*****************************

Dans le fichier ��index.php�� qui se trouve � la racine de l'application 

Juste apr�s ces lignes

/-----------------\
/**
 * Route
 */
$route = array();
\-----------------/

Il faut rajouter le code suivant�:

/----------------------------------\
$route['accueilSport'] = array(
   'c' => "CtrlSport",
   'm' => "getModuleSport",
   'a' => "",
   'module' => "sport"
);
$route['afficherFluxRss'] = array(
   'c' => "CtrlSport",
   'm' => "getInfos",
   'a' => "",
   'module' => "sport"
);
$route['sport'] = array(
   'c' => "CtrlSport",
   'm' => "getSport",
   'a' => "",
   'module' => "sport"
);
$route['afficherSource'] = array(
   'c' => "CtrlSport",
   'm' => "getSources",
   'a' => "",
   'module' => "sport"
);
$route['adminsports'] = array(
   'c' => "CtrlSport",
   'm' => "getAccueilAdministration",
   'a' => "",
   'module' => "sport"
);
$route['ajouterSport'] = array(
   'c' => "CtrlSport",
   'm' => "getFormSport",
   'a' => "",
   'module' => "sport"
);
$route['newSport'] = array(
   'c' => "CtrlSport",
   'm' => "addSport",
   'a' => "",
   'module' => "sport"
);
\----------------------------------/


Ensuite vers la fin du fichier cette ligne

/----------------------------------------------\
$ctrl->$route[$c]['m'](); // $ctrl->getMeteo();
\----------------------------------------------/

Doit �tre remplacer par celle l�

/------------------------------------------------\
$ctrl->$route[$c]['m']($a); // $ctrl->getMeteo();
\------------------------------------------------/

		*****************************
		***    Fichier nav.php    ***
		*****************************

Dans le fichier : themes/default/partials/nav.php

Il faut rajouter le code suivant :

/----------------------------------------------\
<ul class="nav navbar-nav" id="menu-deroulant">
   <li class="dropdown">
      <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sports<span class="caret"></span></a>
      <ul class="dropdown-menu">
         <li><a tabindex="-1" href="accueilSport">Accueil</a>
         <li><a href="adminsports">Administration</a></li>
      </ul>
   </li>
</ul>
\------------------------------------------------/


Entre les codes suivants :

/------------------------------------------------\
<div class="navbar-header">
	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" href="" style="padding:0;">
		<img alt="Logo AFPA" src="themes/<?= Config::$theme?>/images/logo.jpg" style="height:50px;">
	</a>
</div>
\------------------------------------------------/
***INTEGRER LE CODE ICI***
/------------------------------------------------\
<form class="navbar-form navbar-left" action="meteo" method="post">
	<div class="form-group">
		<input type="text" name="ville" class="form-control" placeholder="Saisir une ville">
	</div>
	<button type="submit" class="btn btn-primary">M�t�o</button>
</form>
\------------------------------------------------/




Et voil� votre module Sport est fonctionnel.