<?php
class CtrlSport{

	private $view;
	private $model;
	private $datas;
	//private $newSport;

	public function __construct(){
		$this->view = new ViewSport;
		$this->model = new ModelSport;
		$this->datas = array();
		//$this->newSport = $_POST['newSport'];
	}

	public function getModuleSport($source=""){
		//var_dump($_SESSION['liste_source']);
		$this->model->getListeSources($source);
		$listeSport = $this->model->getListeSports();

		if (empty($source)){
			$_SESSION['source_carousel'] = "http://rmcsport.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/";
		}
		else {
			$this->model->getListeSources($source);
			$_SESSION['source_carousel'] = $_SESSION['liste_source'][$source];
		}
		$this->view->afficherSports($listeSport);
	}

	public function getInfos($sport){
		//	$sport =
		$url = $this->model->getUrl($sport);
		$Rss = $this->model->getRSS($url);
		$this->view->AfficherInfos($Rss);
	}
	private function preparer(){
		return true;
	}

	public function getSport(){
		$sport = array_key_exists('sport', $_POST) ? $_POST['sport'] : false;
		if ($sport==false){
			$this->view->afficherErreurDeSaisie($this->datas,'nonsaisi');
		}
		else {
			if ($this->verifier($sport)== true){
				$this->getInfos($sport);
			}
			else {
				$this->view->afficherErreurDeSaisie($this->datas,'nontrouve');
			}
		}

	}

	public function verifier($sport){
		$verif=false;
		$sport = strtolower($sport);
		$liste = $this->model->getListeSports();
		//var_dump($liste);
		foreach($liste as $v){
			//var_dump($v);
			$v['nom'] = strtolower($v['nom']);
			if(in_array($sport, $v)){
				$verif=true;
			}
		}
		return $verif;
		//	var_dump($verif);
	}
	public function getAccueilAdministration()
	{
		$this->view->afficherAccueilAdmin();
	}

	public function getFormSport(){
		$this->view->afficherFormSport();
	}

	public function addSport(){
		$newSport = $_POST['newSport'];
		//var_dump($newSport);
		$sports = $this->model->getListeSports();
		$verif = $this->verifier($newSport);
		//var_dump($verif);
		if ($verif == false) {
			$ok = $this->model->enregistrerSport($newSport);
			//var_dump($ok);

			if ($ok == true) {
				$this->getFormSport();
			}

			if ($ok == false) {
				$this->view->afficherEnregistrementErreur();
			}
		}
		if($verif == true){
			$this->view->afficherErreurSportDejaExistant();
		}

	}
}
?>