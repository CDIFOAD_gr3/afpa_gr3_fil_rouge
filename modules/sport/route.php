<?php
/**
 * Created by PhpStorm.
 * User: Sam
 * Date: 08/06/2017
 * Time: 11:06
 */
$routes['accueilSport'] = array(
    'c' => "CtrlSport",
    'm' => "getModuleSport",
    'a' => "",
    'module' => "sport"
);
$routes['afficherFluxRss'] = array(
    'c' => "CtrlSport",
    'm' => "getInfos",
    'a' => "",
    'module' => "sport"
);
$routes['sport'] = array(
    'c' => "CtrlSport",
    'm' => "getSport",
    'a' => "",
    'module' => "sport"
);
$routes['afficherSource'] = array(
    'c' => "CtrlSport",
    'm' => "getSources",
    'a' => "",
    'module' => "sport"
);
$routes['adminsports'] = array(
    'c' => "CtrlSport",
    'm' => "getAccueilAdministration",
    'a' => "",
    'module' => "sport"
);
$routes['ajouterSport'] = array(
    'c' => "CtrlSport",
    'm' => "getFormSport",
    'a' => "",
    'module' => "sport"
);
$routes['newSport'] = array(
    'c' => "CtrlSport",
    'm' => "addSport",
    'a' => "",
    'module' => "sport"
);