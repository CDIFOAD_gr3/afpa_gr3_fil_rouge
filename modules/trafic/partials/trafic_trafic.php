<style>@import "<?= Config::$base_href ?>modules/trafic/css/trafic.css"</style>

<script>
    function findPos(obj) {
        var curtop = 0;
        if (obj.offsetParent) {
            do {
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return [curtop];
        }
    }
</script>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<!--<script>
    $(".header").click(function () {

        $header = $(this);
        //getting the next element
        $content = $header.next();
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function () {
                //change text based on condition
                return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

    });
</script>-->

<style>@import "https://fonts.googleapis.com/icon?family=Material+Icons"</style>
<style>@import "https://fonts.googleapis.com/css?family=Montserrat"</style>

<!--<style>@import "https://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css" type="text/css"</style>-->

<div id="container">
<div id="titre">Infos trafic</div>
<a onclick=window.scroll(0,findPos(document.getElementById("titre2"))); style="cursor:pointer">Vers la carte de trajet...</a>
	<div id="fluxRss">
<?php
foreach ($datas['sources'] as $src){
$i = 0; //RaZ à chaque source
$maxRss = 3; // nbre d'article par flux
	if ($datas['errors'][$src[0]]==false){
		foreach ($datas['datas'][$src[0]] as $article){
			$i++;
			if ($i <= $maxRss)
            {
                //affiche les $maxRss premiers de chaque flux
?>

            <button class="btnNews" data-toggle="collapse" data-target="#<?php echo $src[0]."_".$i;?>">
                <?php echo $article['title'];?><p data-toggle="tooltip" data-placement="bottom" title="Lire l'article"><i class="material-icons">arrow_drop_down</i><p></button>
                <div id="<?php echo $src[0]."_".$i;?>" class="collapse">
                    <div class="article" <?php echo $article['date']. $article['desc']?>
                        <?php echo '<a href="'.$article['lnk'].'"target="_blank">Lien article</a>';?></div>

            </div>


<?php
            } //if ($i <= $maxRss)
        } //foreach ($datas['datas'][$src[0]] as $article){
    }//if ($datas['errors'][$src[0]]==false){
} //foreach ($datas['sources'] as $src){

?>
</div>
</div>


    <div id="titre2">Trajets</div>
    <a onclick=window.scroll(0,findPos(document.getElementById("container"))); style="cursor:pointer">Haut de page...</a>



    <div class="pac-card" id="pac-card">
        <div id="myBtn"><button class="btn-lg" id="myBtnMenu" type="button" data-toggle="collapse" data-target="#collapseMenu" aria-expanded="false" aria-controls="collapseMenu">
                <i class="material-icons">menu</i></button></div>
        <div class="collapse" id="collapseMenu">
                <div class="navCarte">
                    <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Géolocalisation" type="button" id="btn_geoloc" value="" onclick="geoloc();">
                        <i class="material-icons">my_location</i></button>
                        <div><input type="text" name="origin" id="origin" placeholder="Départ"></div>
                        <div><input type="text" name="destination" id="destination" placeholder="Destination"></div>

            <button class="btn btn-primary btn-sm" data-toggle="tooltip" title="Calculer l'itirénaire" type="button" value="Calculer l'itinéraire" onclick="javascript:calculate()">
                <i class="material-icons">directions</i></button></div>
            <button class="btn btn-primary btn-sm" data-toggle="tooltip" title="Afficher les étapes" type="button" id="etapes" value="Etapes" onclick="toggle_visibility('panel')" >
                <i class="material-icons">place</i><i class="material-icons">more_horiz</i></button>

        <input id="pac-input" type="text" placeholder="adresse, cinéma, patinoire...">
        </div>
            </div>

<div id="map" style="height: 800px; width: 100%"></div>
<div id="infowindow-content">
    <span id="place-name"  class="title"></span><br>
    <span id="place-address"></span>
</div>
<div id="panel"></div>



    <!-- Include Javascript -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAID1qsUH2d6ajd83Oo6kL9ir5X0X_lF2k&language=fr&libraries=places"></script>
<script type="text/javascript" src="modules/trafic/js/trafic.js"></script>

