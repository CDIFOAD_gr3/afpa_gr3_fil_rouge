<?php
class CtrlTrafic
{
    var $view;
    var $mdl;

    //valeurs par d�faut, pourront �tre mises dans Config;
    //etiquette | url | limite de resultat (-1 = tout)
    var $src = [
        ["autorouteInfo", "http://www.autorouteinfo.fr/rssbreves.ashx",5],
        ["securiteRoutiere", "http://www.securite-routiere.gouv.fr/feed/rss/actualites",5]
    ];

    function __construct()
    {
        $this->view= new ViewTrafic;
        $this->mdl = new ModelTrafic;

        //on �crase avec la config, si elle existe...
        if (isset(Config::$src_xml_trafic)){
            $this->src= Config::$src_xml_trafic;
        }
    }

    function getTrafic(){
        $this->view->afficherTrafic($this->mdl->getNews($this->src));
    }
}
