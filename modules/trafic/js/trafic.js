var map;
var panel;
var initialize;
var calculate;
var direction;
var service;
var infowindow;

// 
var map = new google.maps.Map(document.getElementById('map'), {
  center: {lat: 43.6, lng: 1.43333},
  zoom: 13,
  mapTypeId : google.maps.MapTypeId.TERRAIN
});

var geocoder;

function geoloc(){
  document.getElementById("btn_geoloc");

  infoWindow = new google.maps.InfoWindow;
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                console.log(results)
                if (results[1]) {
         //formatted address
         document.getElementById("origin").value = results[0].formatted_address;
        //find country name
        for (var i=0; i<results[0].address_components.length; i++) {
          for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
            if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                  }
                }
              }
        //city data
        //retourne le département ("occitanie")
        //alert(city.short_name + " " + city.long_name)


      } else {
        alert("No results found");
      }
    } else {
      alert("Geocoder failed due to: " + status);
    }
  });
            infoWindow.setPosition(pos);
            infoWindow.setContent('Vous êtes ici.');
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }

      };


      calculate = function(){
            origin      = document.getElementById('origin').value; // Le point départ
            destination = document.getElementById('destination').value; // Le point d'arrivé
            if(origin && destination){
              var request = {
                origin      : origin,
                destination : destination,
                    travelMode  : google.maps.DirectionsTravelMode.DRIVING // Mode de conduite
                  }
                var directionsService = new google.maps.DirectionsService(); // Service de calcul d'itinéraire
                directionsService.route(request, function(response, status){ // Envoie de la requête pour calculer le parcours
                  if(status == google.maps.DirectionsStatus.OK){
                        direction.setDirections(response); // Trace l'itinéraire sur la carte et les différentes étapes du parcours
                      }
                    });
              }
            };

            function toggle_visibility(id) {
              var e = document.getElementById(id);
              if (e.style.display == 'none' || e.style.display=='') e.style.display = 'block';
              else e.style.display = 'none';
            }

            function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: {lat: 43.6, lng: 1.43333},
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_CENTER
                    },
                    scaleControl: true,
                    streetViewControl: true,
                    streetViewControlOptions: {
                        position: google.maps.ControlPosition.LEFT_CENTER
                    },
                    fullscreenControl: true,
                    fullscreenControlOptions: {
                        position: google.maps.ControlPosition.LEFT_CENTER
                    }

                });

                var trafficLayer = new google.maps.TrafficLayer();
                trafficLayer.setMap(map);



                geocoder = new google.maps.Geocoder();
                var panel = document.getElementById('panel');

              direction = new google.maps.DirectionsRenderer({
                map   : map,
        panel : panel // Dom element pour afficher les instructions d'itinéraire
      });
              var card = document.getElementById('pac-card');
              var input = document.getElementById('pac-input');
              var depart = document.getElementById('origin');
              var arrive = document.getElementById('destination');


              map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

              var autocomplete = new google.maps.places.Autocomplete(input);
              var autocomplete1 = new google.maps.places.Autocomplete(depart);
              var autocomplete2 = new google.maps.places.Autocomplete(arrive);

        // Reliez la propriété de la limite de la carte (viewport) à l'objet autocomplete,
        // afin que les demandes de remplissage automatique utilisent les limites de la carte actuelle pour Option 
        // limites dans la requête.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // L'utilisateur entre un nom de lieu non connu
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });

      }

      initMap();
