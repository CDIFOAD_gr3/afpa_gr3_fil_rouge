$(document).ready(function() {
	
	myAjax({
		'url' 		: "rechercheEmploi",// Envoyé directement dans la barre d'état
		'type' 		: "POST",  // Méthode de récupération
		'dataType' 	: "html", // Type de données
		'data'		: {},
		'callbackSuccess' : function(result){ // result => fichier retourné
			$("#rechercheEmploi").html(result);
		},
		'callbackError' : function(result){
			console.log(result);
		}
	});
});


function myAjax(obj){
	$.ajax({
		url : obj.url,
		type : obj.type,
		dataType : obj.dataType,
		async	:	true,
		data: obj.data,
		success : function(result){
			obj.callbackSuccess(result);
		},
		error : function(result){
			obj.callbackError(result);
		},
		complete : function(result){
			console.log('fini widget recherche');
		}
	});
}
