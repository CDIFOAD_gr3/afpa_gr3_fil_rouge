
/**
	Requete en AJAX pour charger le widget emploi

*/

$(document).ready(function() {
	
	myAjax({
		'url' 		: "widgetemploi",// Envoyé directement dans la barre d'état
		'type' 		: "POST",  // Méthode de récupération
		'dataType' 	: "html", // Type de données
		'data'		: {},
		'callbackSuccess' : function(result){ // result => fichier retourné
			$("#widgetemploi").html(result);
		},
		'callbackError' : function(result){
			console.log(result);
		}
	});
});


function myAjax(obj){
	$.ajax({
		url : obj.url,
		type : obj.type,
		dataType : obj.dataType,
		async	:	true,
		data: obj.data,
		success : function(result){
			obj.callbackSuccess(result);
		},
		error : function(result){
			obj.callbackError(result);
		},
		complete : function(result){
			console.log('fini widget emploi');
		}
	});
}