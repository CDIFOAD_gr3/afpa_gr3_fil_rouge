<?php
/**
 * Created by PhpStorm.
 * User: Sam
 * Date: 08/06/2017
 * Time: 11:05
 */
$routes['emploi'] = array(
    'c' => "CtrlEmploi",
    'm' => "getEmploi",
    'a' => "",
    'module' => "emploi"

);
$routes['rechercheEmploi'] = array(
    'c' => "CtrlEmploi",
    'm' => "getEmploi",
    'a' => "",
    'module' => "emploi"
);
$routes['widgetemploi'] = array(
    'c' => "CtrlEmploi",
    'm' => "getInfo",
    'a' => "",
    'module' => "emploi"
);