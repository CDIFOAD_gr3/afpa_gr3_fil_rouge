Proc�dure d'installation du module emploi.

1) Copier le module emploi dans le r�pertoire "modules".

2) Dans le fichier index.php, rajouter le code suivant :

$route['emploi'] = array(
		'c' => "CtrlEmploi",
		'm' => "getEmploi",
		'a' => "",
		'module' => "emploi"
		
);
$route['rechercheEmploi'] = array(
		'c' => "CtrlEmploi",
		'm' => "getEmploi",
		'a' => "",
		'module' => "emploi"
);
$route['widgetemploi'] = array(
		'c' => "CtrlEmploi",
		'm' => "getInfo",
		'a' => "",
		'module' => "emploi"
);

3) Dans le fichier /themes/default/partials/nav.php, dans  <ul class="dropdown-menu">, 
rajouter :  <li><a href="emploi">Emploi</a></li> 

4) Dans le fichier /themes/default/partials/aside.php, rajouter apr�s le <div id="widgetmeteo"></div>, 
<div id="widgetemploi"></div>


