<?php

class CtrlEmploi{
	
	private $view;
	private $model;
	private $datas;
	private $datasInfo;
	private $dataUrl;
	
	public function __construct(){
		$this->view = new ViewEmploi;
		$this->model = new ModelEmploi;
		$this->datas = array();
		$this->datasInfo = array();
		$this->dataUrl = array('url_depeche' => 'http://www.ladepeche.fr/rss/emploi-formation.rss',
							   'url_Indeed'	 => 'http://www.indeed.fr/rss?');
	
	}
	
	
	
/*	public function verifierInfo(){
		$this->datas = $this->model->infoDepeche();
		$this->view->afficherInfoDepeche($this->datas);
	}*/
	
	private function verifierEmploi($datas){
		//var_dump($datas);
		//$this->view->afficherInfoDepeche($this->datas);
		return true;
	}
	

	
	public function getRecherche($lieu,$poste){

		$this->datas = $this->model->emploi($lieu,$poste,$this->dataUrl['url_Indeed']);

		if ($this->verifierEmploi($this->datas))
			return true;
		else 
			return false;
	}
	
	function getEmploi(){
		$poste="";
		$lieu="";

		$this->view->afficherRecherche();
	
		if (isset($_POST['commune']) && isset($_POST['metier'])){
			$lieu = $_POST['commune'];
			$poste = $_POST['metier'];
			
			if($this->getRecherche($lieu,$poste)){
				$this->view->afficheListeEmploi($this->datas);
				
			}
			else 
				$this->view->afficheErreurEmploi();
		}
		
		$this->view->afficherListeSite();
	}
	
	// WIDGET EMPLOI DEPECHE
	public function getInfo(){

		$this->datasInfo= $this->model->infoDepeche($this->dataUrl['url_depeche']);
		$val = $this->verifierInfo();

		if($val){
			$this->view->afficherInfoDepeche($this->datasInfo);
		}else{
			$this->view->afficherErreurInfo();
		}
	}
	

	

	
	private function verifierInfo(){
		$ok = false;

		if(array_key_exists('erreur',$this->datasInfo)){
			$ok = false;
		}else $ok = true;
		return $ok;
	} // FIN WIDGET EMPLOI DEPECHE
	


	
}
?>