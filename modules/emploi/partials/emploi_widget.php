
<?php
$i = 0;
	$carousel_indicators = "";
	$carousel_inner = "";
?>

 <?php foreach ($datas->channel->item as $item) : ?>
<?php

	if($i == 0){
		$carousel_indicators.= "<li data-target='#mycarousel' data-slide-to=".$i." class='active'></li>\n";
		$carousel_inner.= "<div class='item active'>\n";
	} else {
		$carousel_indicators.= "<li data-target='#mycarousel' data-slide-to='".$i."'></li>\n";
		$carousel_inner.= "<div class='item'>\n";
	}
	$i++;
?>
<?php
	$carousel_inner.= "<a href='".$item->link."'  target='_blank'><img alt='".$item->author."'  src='".$item->enclosure['url']."'><div class='carousel-caption' id='carousel'>";
	$carousel_inner.= "<h5>".$item->title."</h5>";
	$carousel_inner.= "</div></a></div>\n";
?>
		
<?php endforeach; ?>
 <div class="col-xs-10 col-xs-offset-1"> 
	<div class='carousel slide' data-ride='carousel' data-interval='5000' id="mycarousel">
		<!-- Carousel indicators -->
		<ol class='carousel-indicators'>
		<?php echo $carousel_indicators; ?>
		</ol>
		<!-- Carousel items -->
		<div class="carousel-inner" role="listbox">
		<?php echo $carousel_inner; ?>
		</div>
		<!-- Carousel nav -->
		<a class='carousel-control left' href='#mycarousel'  data-slide='prev'>
			<span class='glyphicon glyphicon-chevron-left'></span>
		</a>
		<a class='carousel-control right' href='#mycarousel' data-slide='next'>
			<span class='glyphicon glyphicon-chevron-right'></span>
		</a>
	</div>
</div>  
<style>
 /* https://www.emmanuelbeziat.com/blog/principes-du-css-poids-des-declarations/ */
	

	<!-- Masquer les puces sur les images -->
	.carousel-indicators li{
		border:none;
	}
	.carousel-indicators {

		z-index:0;
	}
	<!-- Titre de l\'article -->
	#carousel h5 {
		font-size:15px;
		background-color: rgba(100, 50, 120, 0.7);
	}
	<!-- bloc du titre article -->
	.carousel-caption {

/* 	    right: 0%;  */
    		/*left: 0%;*/  
/* 	   	padding-bottom: 0px; */
	    background-color: rgba(100, 50, 120, 0.7);
	}
	.carousel-caption {
	
/* 	    position: absolute; */
/* 	    right: 15%; */
/* 	    bottom: 0px; */
/* 	    left: 15%; */
/* 	    z-index: 10; */
	    /* padding-top: 20px; */
	    /* padding-bottom: 20px; */
/* 	    color: #fff; */
/* 	    text-align: center; */
/* 	    text-shadow: 0 1px 2px rgba(0,0,0,.6); */
	}
</style>
