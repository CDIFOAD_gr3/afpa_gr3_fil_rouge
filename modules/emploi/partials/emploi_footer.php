<style>

	div.gallery {
    margin: 15px;
    border: 1px solid #ccc;
    float: left;
    width: 100px;
}
	
	div.gallery img {
    width: 100%;
    height: 50;
}
</style>
	
	


<footer>
 <div class="col-xs-6 col-xs-offset-1">

<div class="gallery"><a href="https://www.apec.fr" target='_blank'><img src = "https://www.apec.fr/modules/apec-template-bootstrap-responsive/images/portail2016/apec-logo.png" usemap= "#APEC" width="100" height="50" title="APEC"></a></div>


<div class="gallery"><a href="http://www.moovijob.com/" target='_blank'><img src = "http://www.moovijob.com/static35db997c36fbb1b63db681786d6abc4ae6adcafa/img/divers/moovijob.svg" usemap = "#MOVIJOB"  width="100" height="50" title="MOVIJOB"></a></div>


<div class="gallery"><a href="https://www.emploi-environnement.com/" target='_blank'><img src = "https://www.emploi-environnement.com/images/interface/header/logo-emploi-environnement-small.png" usemap = "#Environnement"  width="100" height="50" title="Emploi_Environnement"></a></div>

<div class="gallery"><a href="https://talentup.com" target='_blank'><img src = "https://talentup.com/Img/Talentup.png" usemap = "#TALENTUP" width="100" height="50" title="TALENTUP"></a></div>

<div class="gallery"><a href="http://www.ac-toulouse.fr/" target='_blank'><img src = "http://cache.media.education.gouv.fr/image/General/05/6/logo_reg_toulouse_653056.png" usemap = "#AcaToulouse" width="100" height="50" title="Academie de Toulouse"></a></div>

<div class="gallery"><a href="https://www.alsacreations.com/" target='_blank'><img src = "https://cdn.alsacreations.net/css/img/logo-alsacreations.svg" usemap = "#ALSACREATIONS" width="100" height="50" title="Alsacreations"></a></div>

<div class="gallery"><a href="https://www.developpez.net/forums/" target='_blank'><img src = "https://gabarit.developpez.be/images/logo.png" usemap = "#DeveloppezNet"  width="100" height="50" title="Developpez.Net"></a></div>
</div>
</footer>