----------------------------------------
Router v0.1
----------------------------------------

	- chaque module doit contenir un fichier "route.php" à sa racine
		Exemple avec le module veille :
		
			│   route.php
			│
			├───controllers
			│       CtrlVeille.php
			│
			├───models
			│       ModelVeille.php
			│
			├───partials
			│       veille_veille.php
			│
			└───views
					ViewVeille.php
					
		pour ce module le fichier route.php contient :
		
			$routes['veille'] = array(
				'c' => "CtrlVeille",
				'm' => "getVeille",
				'a' => "",
				'module' => "veille"
			);
		pour le module sport, il ressemble à ça :
		
			$routes['accueilSport'] = array(
				'c' => "CtrlSport",
				'm' => "getModuleSport",
				'a' => "",
				'module' => "sport"
			);
			$routes['afficherFluxRss'] = array(
				'c' => "CtrlSport",
				'm' => "getInfos",
				'a' => "",
				'module' => "sport"
			);
			$routes['sport'] = array(
				'c' => "CtrlSport",
				'm' => "getSport",
				'a' => "",
				'module' => "sport"
			);
			$routes['afficherSource'] = array(
				'c' => "CtrlSport",
				'm' => "getSources",
				'a' => "",
				'module' => "sport"
			);
			$routes['adminsports'] = array(
				'c' => "CtrlSport",
				'm' => "getAccueilAdministration",
				'a' => "",
				'module' => "sport"
			);
			$routes['ajouterSport'] = array(
				'c' => "CtrlSport",
				'm' => "getFormSport",
				'a' => "",
				'module' => "sport"
			);
			$routes['newSport'] = array(
				'c' => "CtrlSport",
				'm' => "addSport",
				'a' => "",
				'module' => "sport"
			);
			
//////////////////////////////////////////////////////////
initialisation dans index (Après le include de l'autoloader !!!):

	$router = new Router;
	//à décommenter en cas d'installation d'un nouveau module, puis à recommenter :
	//router->purgeCache(); //efface le fichier cache, force la régénartion au prochain setRoutes();
	$router->setRoutes();
	$route = $router->getRoutes();
	
	
	
//////////////////////////////////////////////////////////
Notes :
	- à l'installation d'un nouveau module, pensez à effacer le fichier 'cache/routes.cache'
	  ou décommentez la ligne dans l'exemple ci-dessus, sinon, les nouvelles routes ne seront pas prises en compte.
	- utilisateurs Linux (voire Mac ?) => /cache doit être inscriptible.

		
