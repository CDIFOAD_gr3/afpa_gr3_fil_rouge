<?php

/**
 * Created by PhpStorm.
 * User: Sam
 * Date: 08/06/2017
 * Time: 10:42
 */
class Router
{
    var $routes;
    var $cache_file = "cache/routes.cache";

    /*
     *  $modulesRoot pourra être changé à "admin" ultérieurement.
     *
    */
    private function getIndividualRoute($modulesRoot = 'modules'){

        //vérifie le trailingslash
        if ($modulesRoot[strlen($modulesRoot)-1] !== '/')
            $modulesRoot=$modulesRoot.'/';

        //renvoie les dossiers contenus dans /modules/
        $road = array_diff(scandir($modulesRoot),array('..', '.'));

        //construit le tableau modules

        //si fichier "route.php" trouvé, on garde le chemin.
        $ro=array();
        foreach ($road as $roa) {
            if (is_dir($modulesRoot.$roa)&& ($roa[0]!=='_')) //un dossier module préfixé d'un underscore est inactif ?
                if (file_exists($modulesRoot.$roa.'/route.php')){
                $ro[]=$modulesRoot.$roa.'/route.php';
            }
        }

        return($ro); //renvoie la liste des fichiers
    }

    public function purgeCache(){
        if (file_exists($this->getCacheFile())){
            unlink($this->getCacheFile());
        }
    }

    public function setRoutes()
    {
        $routes=array();
        //si le fichier cache est introuvable
        if (!file_exists($this->getCacheFile())) {
            //on parcourt la liste des fichiers "route.php"
            foreach ($this->getIndividualRoute() as $k) {
                if ($k !== '') {
                    include_once($k); //on les inclue

                }

            };
            //$routes est en interne, avec les include....
            //on le set dans l'objet
            $this->routes = $routes;
            //et on le met en cache
            file_put_contents($this->getCacheFile(),serialize($routes));
        }
        else{
            //si le fichier cache existe, on le charge
            $this->routes=unserialize(file_get_contents($this->getCacheFile()));
        }
    }

    public function getRoutes()
    {
        return $this->routes;
    }

    public function setCacheFile($cache_file)
    {
        $this->cache_file = $cache_file;
    }

    private function getCacheFile()
    {
        return $this->cache_file;
    }


}